import moment from 'moment'

export const globalTimeFormat = 'DD-MM-YYYY, HH:mm'
export const globalDateFormat = 'DD-MM-YYYY'

export function durationBetweenTwoDate (startDate, endDate) {
  const tempTime = moment.duration(
    moment(endDate, globalTimeFormat).diff(moment(startDate, globalTimeFormat))
  )

  const diff = `${tempTime.hours()}h ${tempTime.minutes()}m`

  return diff
}
