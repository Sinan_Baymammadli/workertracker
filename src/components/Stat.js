import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, StyleSheet } from 'react-native'
import colors from '../styles/colors'
import { durationBetweenTwoDate } from '../helpers/time'

class Stat extends Component {
  render () {
    const { stat } = this.props
    return (
      <View style={styles.stat}>
        <Text style={styles.date}>
          Checkin date:
          {stat.check_in_date}
        </Text>
        <Text style={styles.date}>
          Checkout date:
          {stat.check_out_date}
        </Text>
        <Text style={styles.date}>
          Total time:
          {" "}
          {stat.check_out_date &&
           durationBetweenTwoDate(stat.check_in_date, stat.check_out_date)}
        </Text>
      </View>
    )
  }
}

Stat.propTypes = {
  stat: PropTypes.shape({
    check_in_date: PropTypes.string.isRequired,
    check_out_date: PropTypes.string
  }).isRequired
}

Stat.defaultProps = {
  stat: {
    check_out_date: 'Not finished'
  }
}

export default Stat

const styles = StyleSheet.create({
  stat: {
    marginHorizontal: 16,
    marginTop: 10,
    marginBottom: 15,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.borderColor,
    padding: 10
  },
  date: {
    marginBottom: 5,
    fontWeight: 'bold'
  },
  time: {
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: colors.borderColor
  },
  totalTime: {
    fontWeight: 'bold',
    paddingVertical: 10
  }
})
