import firebase from 'react-native-firebase'

export default function uploadImage (image) {
  const { currentUser } = firebase.auth()
  const seconds = new Date().getTime()
  const name = `${seconds}.jpg`
  const metadata = {
    contentType: 'image/jpeg'
  }

  return new Promise((resolve, reject) => {
    firebase
      .storage()
      .ref()
      .child(`${currentUser.email}/${name}`)
      .putFile(image.uri, metadata)
      .then(uploadedImage => {
        resolve(uploadedImage.downloadURL)
      })
      .catch(error => {
        reject(error)
      })
  })
}
