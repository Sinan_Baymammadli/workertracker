import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { Text, Input, Button } from "react-native-elements";
import firebase from "react-native-firebase";

class Login extends Component {
  state = {
    email: "",
    password: "",
    loading: false,
    error: ""
  };

  login = () => {
    this.setState({
      loading: true
    });

    const { email, password } = this.state;

    if (email === "" || password === "") {
      this.setState({
        loading: false,
        error: "Fields are required."
      });
      return;
    }

    firebase
      .auth()
      .signInAndRetrieveDataWithEmailAndPassword(email, password)
      .catch(error => {
        this.setState({
          loading: false,
          error: error.message
        });
      });
  };

  render() {
    const { email, password, loading, error } = this.state;

    return (
      <View style={styles.container}>
        <Text h2 style={styles.welcome}>
          Login
        </Text>
        <Text style={{ color: "red", textAlign: "center" }}>{error}</Text>
        <Input
          placeholder="Email"
          label="Email"
          returnKeyType="next"
          autoCapitalize="none"
          keyboardType="email-address"
          containerStyle={{
            marginBottom: 15,
            width: "100%"
          }}
          value={email}
          onChangeText={text => this.setState({ email: text.trim() })}
        />

        <Input
          placeholder="Password"
          label="Password"
          autoCapitalize="none"
          returnKeyType="go"
          secureTextEntry
          containerStyle={{
            marginBottom: 15,
            width: "100%"
          }}
          value={password}
          onChangeText={text => this.setState({ password: text.trim() })}
          onSubmitEditing={this.login}
        />
        <Button
          title="LOGIN"
          onPress={this.login}
          disabled={loading}
          loading={loading}
          loadingProps={{ size: "large", color: "rgba(111, 202, 186, 1)" }}
          buttonStyle={{
            height: 45
          }}
        />
      </View>
    );
  }
}

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#fff",
    paddingHorizontal: 16
  },
  welcome: {
    textAlign: "center",
    margin: 10
  }
});
