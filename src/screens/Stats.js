import React, { Component } from "react";
import { StyleSheet, View, FlatList, ActivityIndicator } from "react-native";
import { Text } from "react-native-elements";
import firebase from "react-native-firebase";
import Stat from "../components/Stat";

class Stats extends Component {
  static navigationOptions = () => ({
    title: "Statistics"
  });

  state = {
    loading: true,
    refreshing: false,
    stats: [],
    error: ""
  };

  componentDidMount = () => {
    this.getStats();
  };

  getStats = () => {
    this.setState({
      refreshing: true
    });
    const { uid } = firebase.auth().currentUser;
    firebase
      .database()
      .ref(`checker_daily/${uid}`)
      .once("value")
      .then(snap => {
        this.setState({
          loading: false,
          refreshing: false,
          error: "",
          stats: Object.values(snap.val())
        });
      })
      .catch(error => {
        this.setState({
          loading: false,
          refreshing: false
        });
      });
  };

  _keyExtractor = index => index.toString();

  _renderHourlyItem = ({ item }) => <Stat stat={item} />;

  _keyExtractorHourly = item => item.check_in_image_url;

  _renderItem = ({ item }) => {
    const hourlyStats = Object.values(item);
    return (
      <FlatList
        data={hourlyStats}
        keyExtractor={this._keyExtractorHourly}
        renderItem={this._renderHourlyItem}
      />
    );
  };

  render() {
    const { loading, stats, refreshing, error } = this.state;

    return (
      <View style={styles.screen}>
        <Text
          style={{
            marginLeft: 16,
            marginBottom: 10,
            textAlign: "center",
            color: "red"
          }}
        >
          {error}
        </Text>
        {loading ? (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <ActivityIndicator size="large" />
          </View>
        ) : (
          <FlatList
            data={stats}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            onRefresh={this.getStats}
            refreshing={refreshing}
            ListEmptyComponent={
              <Text
                style={{
                  textAlign: "center"
                }}
              >
                No record.
              </Text>
            }
          />
        )}
      </View>
    );
  }
}

export default Stats;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#fff"
  },
  welcome: {
    margin: 10
  },
  noStat: {
    textAlign: "center",
    marginTop: 20,
    fontSize: 16
  }
});
