import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  View,
  AsyncStorage,
  ActivityIndicator,
  TextInput,
  Alert
} from "react-native";
import { Button, Card, Text } from "react-native-elements";
import Modal from "react-native-modal";
import moment from "moment";
import Feather from "react-native-vector-icons/dist/Feather";
import MaterialCommunityIcons from "react-native-vector-icons/dist/MaterialCommunityIcons";
import BackgroundTask from "react-native-background-task";

import { RNCamera } from "react-native-camera";

import { globalDateFormat, globalTimeFormat } from "../helpers/time";
import firebase from "react-native-firebase";
import uploadImage from "../components/uploadImage";

const NEW_CHECKIN_PATH = "newCheckinPath";

BackgroundTask.define(async () => {
  const todayDate = moment().format(globalDateFormat);
  const todayTime = moment().format(globalTimeFormat);
  const uid = await AsyncStorage.getItem("uid");
  const newCheckInRef = firebase
    .database()
    .ref()
    .child(`checker_hourly/${uid}/${todayDate}/${todayTime}`);

  navigator.geolocation.getCurrentPosition(
    position => {
      const location = `${position.coords.latitude}, ${
        position.coords.longitude
      }`;

      newCheckInRef.update({
        date: moment().format(globalTimeFormat),
        location
      });
    },
    error => {}
  );

  BackgroundTask.finish();
});

class Home extends Component {
  static navigationOptions = () => ({
    title: "Home"
  });

  state = {
    appLoading: true,
    checkinLoading: false,
    checkoutLoading: false,
    checkinDisabled: false,
    checkoutDisabled: true,
    modalVisible: false,
    checkoutNote: "",
    error: null,
    locationNotEnabledError: false,
    log: ""
  };

  componentDidMount = async () => {
    try {
      this.newCheckinPath = await AsyncStorage.getItem(NEW_CHECKIN_PATH);
      const { uid } = firebase.auth().currentUser;
      await AsyncStorage.setItem("uid", uid);

      if (this.newCheckinPath) {
        this.setState({
          appLoading: false,
          checkinDisabled: true,
          checkoutDisabled: false
        });
      } else {
        this.setState({
          appLoading: false
        });
      }
    } catch (error) {
      console.log(error);
    }

    navigator.geolocation.getCurrentPosition(
      position => {
        return;
      },
      error => {
        return;
      }
    );
  };

  async checkStatus() {
    const status = await BackgroundTask.statusAsync();

    if (status.available) {
      return;
    }

    const reason = status.unavailableReason;
    if (reason === BackgroundTask.UNAVAILABLE_DENIED) {
      Alert.alert(
        "Denied",
        'Please enable background "Background App Refresh" for this app'
      );
    } else if (reason === BackgroundTask.UNAVAILABLE_RESTRICTED) {
      Alert.alert(
        "Restricted",
        "Background tasks are restricted on your device"
      );
    }
  }

  writeCheckIn = async (location, imgUrl) => {
    const todayDate = moment().format(globalDateFormat);
    const { uid } = firebase.auth().currentUser;
    const newCheckInRef = firebase
      .database()
      .ref()
      .child(`checker_daily/${uid}/${todayDate}`);

    const newCheckInPath = newCheckInRef.push({
      check_in_date: moment().format(globalTimeFormat),
      check_in_location: location,
      check_in_image_url: imgUrl
    });

    if (newCheckInPath) {
      await AsyncStorage.setItem("newCheckinPath", newCheckInPath.path);
      this.newCheckinPath = newCheckInPath.path;
    }

    return newCheckInPath;
  };

  takePicture = () => {
    if (this.camera) {
      const options = { quality: 0.5 };
      return this.camera.takePictureAsync(options);
    }
  };

  checkin = () => {
    this.setState(
      {
        checkinLoading: true
      },
      () => {
        navigator.geolocation.getCurrentPosition(
          async position => {
            try {
              console.log("checkin started");
              const image = await this.takePicture();
              const imgUrl = await uploadImage(image);
              const location = `${position.coords.latitude}, ${
                position.coords.longitude
              }`;
              await this.writeCheckIn(location, imgUrl);

              this.setState({
                checkinLoading: false,
                checkinDisabled: true,
                checkoutDisabled: false,
                locationNotEnabledError: false
              });

              BackgroundTask.schedule({
                period: 900
              });
              this.checkStatus();
            } catch (error) {
              this.setState({
                checkinLoading: false,
                error
              });
            }
          },
          error => {
            if (error.code === 2) {
              this.setState({
                checkinLoading: false,
                locationNotEnabledError: true
              });
            } else {
              this.setState({
                checkinLoading: false,
                error
              });
            }
          }
        );
      }
    );
  };

  writeCheckOut = (location, imgUrl) => {
    const todayDate = moment().format(globalDateFormat);
    const { uid } = firebase.auth().currentUser;
    const { checkoutNote } = this.state;
    console.log(this.newCheckinPath);
    const newCheckInRef = firebase.database().ref(this.newCheckinPath);

    return newCheckInRef.update({
      check_out_date: moment().format(globalTimeFormat),
      check_out_location: location,
      check_out_image_url: imgUrl,
      check_out_note: checkoutNote
    });
  };

  checkout = () => {
    this.setState(
      {
        checkoutLoading: true
      },
      () => {
        navigator.geolocation.getCurrentPosition(
          async position => {
            try {
              this.setState({ log: "started" });
              const image = await this.takePicture();
              console.log("pic taken");
              this.setState({ log: "pic taken" });
              const imgUrl = await uploadImage(image);
              console.log("image uploaded");
              this.setState({ log: "image uploaded" });
              const location = `${position.coords.latitude}, ${
                position.coords.longitude
              }`;
              await this.writeCheckOut(location, imgUrl);
              console.log("info written to db");
              this.setState({ log: "info written to db" });
              await AsyncStorage.removeItem(NEW_CHECKIN_PATH);
              console.log("new checkin removed");
              this.setState({ log: "new checkin removed" });

              this.setState({
                checkoutLoading: false,
                checkoutDisabled: true,
                checkinDisabled: false,
                locationNotEnabledError: false,
                checkoutNote: "",
                modalVisible: false
              });

              BackgroundTask.cancel();
            } catch (error) {
              this.setState({
                checkoutLoading: false,
                error
              });
            }
          },
          error => {
            if (error.code === 2) {
              this.setState({
                checkoutLoading: false,
                locationNotEnabledError: true
              });
            } else {
              this.setState({
                checkoutLoading: false,
                error
              });
            }
          }
        );
      }
    );
  };

  openModal = () => {
    this.setState({
      modalVisible: true
    });
  };

  closeModal = () => {
    this.setState({
      modalVisible: false
    });
  };

  render() {
    const {
      appLoading,
      checkinLoading,
      checkoutLoading,
      checkinDisabled,
      checkoutDisabled,
      modalVisible,
      checkoutNote,
      locationNotEnabledError,
      error
    } = this.state;

    return (
      <View style={styles.screen}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.camera}
          type={RNCamera.Constants.Type.front}
          flashMode={RNCamera.Constants.FlashMode.off}
          permissionDialogTitle={"Permission to use camera"}
          permissionDialogMessage={
            "We need your permission to use your camera phone"
          }
        />
        <View
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "#fff"
          }}
        >
          <View style={styles.container}>
            <Card>
              {appLoading ? (
                <ActivityIndicator />
              ) : (
                <View>
                  <Button
                    title="Checkin"
                    onPress={this.checkin}
                    buttonStyle={{
                      backgroundColor: "#03A9F4",
                      height: 50,
                      borderRadius: 5
                    }}
                    icon={
                      <Feather name="check-circle" size={25} color="white" />
                    }
                    containerStyle={{ marginBottom: 20 }}
                    disabled={checkinLoading || checkinDisabled}
                    loading={checkinLoading}
                    loadingProps={{
                      size: "large",
                      color: "#03A9F4"
                    }}
                  />
                  <Button
                    title="Checkout"
                    onPress={this.openModal}
                    buttonStyle={{
                      backgroundColor: "red",
                      height: 50,
                      borderRadius: 5
                    }}
                    icon={
                      <MaterialCommunityIcons
                        name="logout"
                        size={25}
                        color="white"
                      />
                    }
                    disabled={checkoutLoading || checkoutDisabled}
                    loading={checkoutLoading}
                    loadingProps={{
                      size: "large",
                      color: "red"
                    }}
                  />
                </View>
              )}

              <Text style={styles.error}>
                {locationNotEnabledError && "Please turn on your location."}
              </Text>
            </Card>

            {/* for debuging */}
            {/* <Text>
              {this.state.log && JSON.stringify(this.state.log, null, 2)}
            </Text> */}

            <Modal isVisible={modalVisible} onBackdropPress={this.closeModal}>
              <View
                style={{
                  backgroundColor: "white",
                  padding: 22,
                  borderRadius: 4,
                  borderColor: "rgba(0, 0, 0, 0.1)"
                }}
              >
                <Text>Write any notes here:</Text>
                <TextInput
                  placeholder="Your note..."
                  multiline
                  numberOfLines={5}
                  style={{
                    textAlignVertical: "top",
                    marginVertical: 10,
                    height: 100
                  }}
                  value={checkoutNote}
                  onChangeText={text =>
                    this.setState({
                      checkoutNote: text
                    })
                  }
                />

                <Text style={styles.error}>
                  {locationNotEnabledError && "Please turn on your location."}
                </Text>

                <Button
                  onPress={this.checkout}
                  disabled={checkoutLoading || checkoutDisabled}
                  loading={checkoutLoading}
                  loadingProps={{
                    size: "large",
                    color: "#03A9F4"
                  }}
                  title="Save"
                />
              </View>
            </Modal>
          </View>
        </View>
      </View>
    );
  }
}

Home.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired
  }).isRequired
};

export default Home;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#fff"
  },
  camera: {
    flex: 1
  },
  error: {
    color: "red",
    textAlign: "center",
    marginVertical: 7
  }
});
