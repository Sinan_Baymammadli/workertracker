import React, { Component } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { ListItem } from "react-native-elements";
import MaterialCommunityIcons from "react-native-vector-icons/dist/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/dist/MaterialIcons";
import firebase from "react-native-firebase";

class Settings extends Component {
  static navigationOptions = () => ({
    title: "Settings"
  });

  state = {
    settings: [
      {
        title: "Logout",
        icon: <MaterialCommunityIcons name="logout" size={25} />,
        onPress: () => {
          firebase
            .auth()
            .signOut()
            .catch(error => {
              // An error happened.
              console.log(error);
            });
        }
      }
    ],
    email: null
  };

  componentDidMount = () => {
    const { email } = firebase.auth().currentUser;
    this.setState({
      email
    });
  };

  _keyExtractor = item => item.title;

  _renderItem = ({ item }) => (
    <ListItem
      title={item.title}
      leftIcon={item.icon}
      onPress={item.onPress}
      bottomDivider
    />
  );

  render() {
    const { settings, email } = this.state;

    return (
      <View style={styles.screen}>
        <ListItem
          title={email}
          leftIcon={<MaterialIcons name="email" size={25} />}
          bottomDivider
        />
        <FlatList
          keyExtractor={this._keyExtractor}
          data={settings}
          renderItem={this._renderItem}
        />
      </View>
    );
  }
}

export default Settings;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#fff"
  },
  welcome: {
    margin: 10
  }
});
