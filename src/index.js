import React, { Component } from "react";
import { StyleSheet, ActivityIndicator, View, YellowBox } from "react-native";
import firebase from "react-native-firebase";
import { createRootNavigator } from "./router";
import SplashScreen from "react-native-splash-screen";

// react native navigation issue
YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader",
  "Module RNBackgroundFetch"
]);

export default class App extends Component {
  state = {
    user: null,
    loading: true
  };

  componentDidMount = () => {
    SplashScreen.hide();
    this.unsubscriber = firebase.auth().onAuthStateChanged(async user => {
      this.setState({
        user,
        loading: false
      });
    });
  };

  componentWillUnmount = () => {
    if (this.unsubscriber) {
      this.unsubscriber();
    }
  };

  render() {
    const { loading, user } = this.state;

    if (loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" />
        </View>
      );
    }

    const RootNavigator = createRootNavigator(user);
    return <RootNavigator />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  }
});
