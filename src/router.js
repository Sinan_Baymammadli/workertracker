import React from 'react'
import { createStackNavigator, createBottomTabNavigator, createSwitchNavigator } from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons'
import { StackActions, NavigationActions } from 'react-navigation'

import Login from './screens/Login'
import Home from './screens/Home'
import Settings from './screens/Settings'
import Stats from './screens/Stats'

export const Guest = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  }
})

const HomeStack = createStackNavigator({
  Home: {
    screen: Home
  }
})

const StatsStack = createStackNavigator({
  Stats: {
    screen: Stats
  }
})

const SetingsStack = createStackNavigator({
  Settings: {
    screen: Settings
  }
})

const goBackToHome = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'User' })],
  key: null
})

export const User = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: () => ({
        title: 'Home',
        tabBarIcon: ({ tintColor }) => {
          return <MaterialIcons name='home' size={25} color={tintColor} />
        },
        tabBarOnPress: ({ navigation }) => {
          navigation.dispatch(goBackToHome)
        }
      })
    },
    Stats: {
      screen: StatsStack,
      navigationOptions: () => ({
        title: 'Statistics',
        tabBarIcon: ({ tintColor }) => {
          return <MaterialIcons name='equalizer' size={25} color={tintColor} />
        }
      })
    },
    Settings: {
      screen: SetingsStack,
      navigationOptions: () => ({
        title: 'Settings',
        tabBarIcon: ({ tintColor }) => {
          return <MaterialIcons name='settings' size={25} color={tintColor} />
        }
      })
    }
  },
  {
    initialRouteName: 'Home'
  }
)

const UserStack = createStackNavigator({
  User: {
    screen: User,
    navigationOptions: {
      header: null
    }
  }
})

export const createRootNavigator = (loggedIn = false) => {
  return createSwitchNavigator(
    {
      Guest: {
        screen: Guest
      },
      User: {
        screen: UserStack
      }
    },
    {
      headerMode: 'none',
      initialRouteName: loggedIn ? 'User' : 'Guest'
    }
  )
}
